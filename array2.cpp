#include <iostream>
#include <cmath>
using namespace std;
int main(){
	int t;
	cin >> t;
	long n,dem;
	long long m;
	while(t--){
		cin >> n;
		dem=0;
		for(int i=0 ; i<n ; i++){
			cin >> m;
			if (m>0) cout << m << " ";
			else dem++;
		}
		while(dem--){
			cout << "0 ";
		}
		cout <<endl;
	}
	return 0;
}
